# coding=utf-8
"""
Read Ubigeo data from CSV and convert to a JSON fixture
  ready to load to a Django model

Authors:
    Carlos Joel <cj@carlosjoel.net>

(c) 2016 - FreeBSD License
"""
import json


def write_json_file(file_name, data):
    """
    Writes a file with json data

    :param file_name: Name of the file
    :param data: Data to write into the file
    """
    with open(file_name, 'w') as file_save:
        file_save.write(json.dumps(data))


def load_departments(model_name):
    """
    Loads departments data from CSV and generate JSON

    :param model_name: Model name from Django app
    """
    data = []

    with open('files/departments.csv') as fdep:
        i = 1
        for line in fdep.readlines():
            code = line.split(',')[0]
            name = line.split(',')[1][:-1]
            data.append(
                {
                    'pk': i,
                    'model': model_name,
                    'fields':
                        {
                            'code': code,
                            'name': name,
                        }
                })
            i += 1

    write_json_file('fixtures/departments.json', data)


def load_provinces(model_name):
    """
    Loads provinces data from CSV and generate JSON

    :param model_name: Model name from Django app
    """
    data = []

    with open('files/provinces.csv') as fdep:
        i = 1
        for line in fdep.readlines():
            code = line.split(',')[0]
            department_code = code[:2]
            name = line.split(',')[1][:-1]
            data.append(
                {
                    'pk': i,
                    'model': model_name,
                    'fields':
                        {
                            'code': code,
                            'name': name,
                            'department_code': department_code
                        }
                })
            i += 1

    write_json_file('fixtures/provinces.json', data)


def load_districts(model_name):
    """
    Loads districts data from CSV and generate JSON

    :param model_name: Model name from Django app
    """
    data = []

    with open('files/districts.csv') as fdep:
        i = 1
        for line in fdep.readlines():
            code = line.split(',')[0]
            department_code = code[:2]
            province_code = code[:4]
            name = line.split(',')[1][:-1]
            data.append(
                {
                    'pk': i,
                    'model': model_name,
                    'fields':
                        {
                            'code': code,
                            'name': name,
                            'department_code': department_code,
                            'province_code': province_code
                        }
                })
            i += 1

    write_json_file('fixtures/districts.json', data)


if __name__ == "__main__":
    MODEL_BASE_APP_NAME = 'ubigeo'
    load_departments('{}.departments'.format(MODEL_BASE_APP_NAME))
    load_provinces('{}.provinces'.format(MODEL_BASE_APP_NAME))
    load_districts('{}.districts'.format(MODEL_BASE_APP_NAME))
