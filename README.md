Read "Ubigeo" data from CSV and convert to a JSON fixture ready to load to a Django model
-

Authors:

    * Carlos Joel <cj@carlosjoel.net>

License: **FreeBSD**

(c) 2016
